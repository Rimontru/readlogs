// Require libraries
const
http = require('http'),
fs = require('fs'),
xl = require('excel4node');

let 
files = ['BN1903L2100','BN1904L2100','BN1905L2100','BN1906L2100','BN1907L2100'],
path_source = "./src/resources/",
ARCHIVO_PRUEBA = path_source+'BN1903L2100.log', obj=[];

  // Create a new instance of a Workbook class
  var wb = new xl.Workbook();

  var 
  style = wb.createStyle({ font: { size: 16, weight: 'bold'},}),
  style2 = wb.createStyle({ font: { size: 12}});

  // Add Worksheets to the workbook
  var ws = wb.addWorksheet('Tarjetería'),
      head1 = [
      'RADIOBASE',
      'SMN',
      'APN',
      'BOARD',
      'SWALLOCATION',
      'S',
      'FAULT',
      'OPER',
      'MAINT',
      'STAT',
      'c/p',
      'd',
      'PRODUCTNUMBER',
      'REV',
      'SERIAL',
      'DATE',
      'TEMP',
      'UPT',
      'MO'], a1=1, fi1=2;

  // CREAMOS EL ENCABEZADO DEL ARCHIVO
  for(let a=0; a<=head1.length; a++)
    ws.cell(1,a1++).string(head1[a]).style(style);

  // CARGAMOS LA INFORMACION DE LOS .log
  for (var f1 = 0; f1 < files.length; f1++) 
  {
    ws.cell(fi1, 1).string(files[f1]).style(style2);

    filename = path_source+files[f1]+'.log';

    archivo = fs.readFileSync(filename, 'utf-8');

    parts_file = archivo.split("\n")

    for (var i = 0; i < parts_file.length; i++) 
    {
      if(i==81){
        linea = parts_file[i];
        parts_linea = linea/*.replace(/ /gi,'|')*/.split("  ");
        //console.log(parts_linea);
        ws.cell(fi1, 2).string(parts_linea[1]).style(style2);
        ws.cell(fi1, 3).string(parts_linea[2]).style(style2);
        ws.cell(fi1, 4).string(parts_linea[3]).style(style2);
        ws.cell(fi1, 5).string(parts_linea[4]).style(style2);
        ws.cell(fi1, 6).string(parts_linea[9]).style(style2);
        ws.cell(fi1, 7).string(parts_linea[10]).style(style2);
        ws.cell(fi1, 8).string(parts_linea[11]).style(style2);
        ws.cell(fi1, 9).string(parts_linea[12]).style(style2);
        ws.cell(fi1, 10).string(parts_linea[13]).style(style2);
        parts_linea2 = parts_linea[15].split(" ");
        ws.cell(fi1, 11).string(parts_linea2[0]).style(style2);
        ws.cell(fi1, 12).string(parts_linea2[1]).style(style2);
        ws.cell(fi1, 13).string(parts_linea[16]).style(style2);
        ws.cell(fi1, 14).string(parts_linea[17]).style(style2);
        parts_linea3 = parts_linea[18].split(" ");
        ws.cell(fi1, 15).string(parts_linea3[1]).style(style2);
        ws.cell(fi1, 16).string(parts_linea3[2]).style(style2);
        parts_linea4 = parts_linea[19].split(" ");
        ws.cell(fi1, 17).string(parts_linea4[0]).style(style2);
        ws.cell(fi1, 18).string(parts_linea4[1]).style(style2);
        ws.cell(fi1, 19).string(parts_linea4[2]).style(style2);
      }
    }

    fi1++
  }


//-------------------- HOJA 2 --------------------//
  var ws2 = wb.addWorksheet('Materiales'),
      head2 = [
      'RADIOBASE',
      'XPBOARD',
      'ST',
      'FAULT',
      'OPER',
      'MAINT',
      'PRODUCTNUMBER',
      'REV',
      'SERIAL/NAME',
      'DATE',
      'TEMP',
      'UPT',
      'MO (LNH)'], b1=1, fi2=2;


  for(let b=0; b<=head2.length; b++)
    ws2.cell(1,b1++).string(head2[b]).style(style);

  // CARGAMOS LA INFORMACION DE LOS .log
  for (var f2 = 0; f2 < files.length; f2++) 
  {

    filename = path_source+files[f2]+'.log';

    archivo2 = fs.readFileSync(filename, 'utf-8');

    parts_file2 = archivo2.split("\n")

    for (var i = 0; i < parts_file2.length; i++) 
    {
      if(f2 == 0)
        if(i>=87 && i<=96){
          ws2.cell(fi2, 1).string(files[f2]).style(style2);
          linea2 = parts_file2[i];
          parts_linea5 = linea2.split("  ");
          if(fi2>=2 && fi2<=4){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[1]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[6]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[8]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[12]).style(style2);
          parts_linea6 = parts_linea5[13].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2>=5 && fi2<=7){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[7]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[9]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[13]).style(style2);
          parts_linea6 = parts_linea5[14].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2==8){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[19]).style(style2);
          ws2.cell(fi2, 11).string(parts_linea5[21]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[24]).style(style2);
          }
          if(fi2==9){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[40]).style(style2);
          }
          if(fi2==10 ){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[23]).style(style2);
          }
          if(fi2==11){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[21]).style(style2);
          }

          fi2++
        }
      if(f2 == 1)
        if(i>=87 && i<=92){
          ws2.cell(fi2, 1).string(files[f2]).style(style2);
          linea2 = parts_file2[i];
          parts_linea5 = linea2.split("  ");
          if(fi2>=12 && fi2<=13){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[1]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[6]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[8]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[9]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[11]).style(style2);
          parts_linea6 = parts_linea5[12].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2==14){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[14]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[16]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[18]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[20]).style(style2);
          ws2.cell(fi2, 11).string(parts_linea5[22]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[25]).style(style2);
          }
          if(fi2==15){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[40]).style(style2);
          }
          if(fi2==16){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[23]).style(style2);
          }
          if(fi2==17){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[21]).style(style2);
          }

          fi2++
        }
      if(f2 == 2)
        if(i>=87 && i<=96){
          ws2.cell(fi2, 1).string(files[f2]).style(style2);
          linea2 = parts_file2[i];
          parts_linea5 = linea2.split("  ");
          if(fi2>=18 && fi2<=20){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[1]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[6]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[8]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[12]).style(style2);
          parts_linea6 = parts_linea5[13].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2>=21 && fi2<=23){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[7]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[9]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[13]).style(style2);
          parts_linea6 = parts_linea5[14].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2==24){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[19]).style(style2);
          ws2.cell(fi2, 11).string(parts_linea5[21]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[24]).style(style2);
          }
          if(fi2==25){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[40]).style(style2);
          }
          if(fi2==26){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[23]).style(style2);
          }
          if(fi2==27){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[21]).style(style2);
          }
          
          fi2++
        }
      if(f2 == 3)
        if(i>=87 && i<=94){
          ws2.cell(fi2, 1).string(files[f2]).style(style2);
          linea2 = parts_file2[i];
          parts_linea5 = linea2.split("  ");
          if(fi2>=28 && fi2<=29){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[1]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[6]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[8]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[12]).style(style2);
          parts_linea6 = parts_linea5[13].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2>=30 && fi2<=31){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[7]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[9]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[13]).style(style2);
          parts_linea6 = parts_linea5[14].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2==32){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[19]).style(style2);
          ws2.cell(fi2, 11).string(parts_linea5[21]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[24]).style(style2);
          }
          if(fi2==33){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[40]).style(style2);
          }
          if(fi2==34){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[23]).style(style2);
          }
          if(fi2==35){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[21]).style(style2);
          }

          fi2++
        }
      if(f2 == 4)
        if(i>=87 && i<=93){
          ws2.cell(fi2, 1).string(files[f2]).style(style2);
          linea2 = parts_file2[i];
          parts_linea5 = linea2.split("  ");
          //console.log(parts_linea5)
          if(fi2>=36 && fi2<=38){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[1]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[2]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 6).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[6]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[8]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[12]).style(style2);
          parts_linea6 = parts_linea5[13].split(" ");
          ws2.cell(fi2, 11).string(parts_linea6[0]).style(style2);
          ws2.cell(fi2, 12).string(parts_linea6[1]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea6[2]).style(style2);
          }
          if(fi2==39){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[19]).style(style2);
          ws2.cell(fi2, 11).string(parts_linea5[21]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[24]).style(style2);
          }
          if(fi2==40){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[40]).style(style2);
          }
          if(fi2==41){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[17]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[23]).style(style2);
          }
          if(fi2==42){
          ws2.cell(fi2, 2).string(parts_linea5[0]).style(style2);
          ws2.cell(fi2, 3).string(parts_linea5[3]).style(style2);
          ws2.cell(fi2, 4).string(parts_linea5[4]).style(style2);
          ws2.cell(fi2, 5).string(parts_linea5[5]).style(style2);
          ws2.cell(fi2, 7).string(parts_linea5[10]).style(style2);
          ws2.cell(fi2, 8).string(parts_linea5[11]).style(style2);
          ws2.cell(fi2, 9).string(parts_linea5[13]).style(style2);
          ws2.cell(fi2, 10).string(parts_linea5[15]).style(style2);
          ws2.cell(fi2, 13).string(parts_linea5[21]).style(style2);
          }

          fi2++
        }
    }

    
  }

//-------------------- HOJA 3 --------------------//
  var ws3 = wb.addWorksheet('Licenciamiento Features'),
      head3 = [
      'Radiobase',
      'Feature',
      'keyId',
      'FAJ',
      'LicenseState',
      'FeatureState',
      'ServiceState',
      'ValidFrom',
      'ValidUntil',
      'Description'],c1=1,fi3=2;;

  for(let c=0; c<=head3.length; c++)
    ws3.cell(1,c1++).string(head3[c]).style(style);
  
  // CARGAMOS LA INFORMACION DE LOS .log
  for (var f2 = 0; f2 < files.length; f2++) 
  {

    filename = path_source+files[f2]+'.log';

    archivo = fs.readFileSync(filename, 'utf-8');

    parts_file = archivo.split("\n")

    for (var i = 0; i < parts_file2.length; i++) 
    {
      if(f2 == 0) //ARCHIVO
        if(i>=219 && i<=248){ //SECCION
          //console.log(parts_linea)
          ws3.cell(fi3, 1).string(files[f2]).style(style2);
          linea = parts_file[i];
          parts_linea = linea.split("  ");
          col = 2;
          for (let xy=0; xy<=parts_linea.length; xy++){
            if(parts_linea[xy] == '')
              continue;
            else{
              ws3.cell(fi3, col).string(parts_linea[xy]).style(style2);
              col++;
            }
          }
          
          fi3++
        }
      if(f2 == 1)
        if(i>=176 && i<=205){
          ws3.cell(fi3, 1).string(files[f2]).style(style2);
          linea = parts_file[i];
          parts_linea = linea.split("  ");
          col = 2;
          for (let xy=0; xy<=parts_linea.length; xy++){
            if(parts_linea[xy] == '')
              continue;
            else{
              ws3.cell(fi3, col).string(parts_linea[xy]).style(style2);
              col++;
            }
          }
          
          fi3++
        }
      if(f2 == 2)
        if(i>=219 && i<=248){
          ws3.cell(fi3, 1).string(files[f2]).style(style2);
          linea = parts_file[i];
          parts_linea = linea.split("  ");
          col = 2;
          for (let xy=0; xy<=parts_linea.length; xy++){
            if(parts_linea[xy] == '')
              continue;
            else{
              ws3.cell(fi3, col).string(parts_linea[xy]).style(style2);
              col++;
            }
          }
          
          fi3++
        }
      if(f2 == 3)
        if(i>=196 && i<=225){
          ws3.cell(fi3, 1).string(files[f2]).style(style2);
          linea = parts_file[i];
          parts_linea = linea.split("  ");
          col = 2;
          for (let xy=0; xy<=parts_linea.length; xy++){
            if(parts_linea[xy] == '')
              continue;
            else{
              ws3.cell(fi3, col).string(parts_linea[xy]).style(style2);
              col++;
            }
          }
          
          fi3++
        }
      if(f2 == 4)
        if(i>=189 && i<=218){
          ws3.cell(fi3, 1).string(files[f2]).style(style2);
          linea = parts_file[i];
          parts_linea = linea.split("  ");
          col = 2;
          for (let xy=0; xy<=parts_linea.length; xy++){
            if(parts_linea[xy] == '')
              continue;
            else{
              ws3.cell(fi3, col).string(parts_linea[xy]).style(style2);
              col++;
            }
          }
          
          fi3++
        }/**/
    }
  }

wb.write('DatosLog.xlsx');